<nav class="fh5co-nav" role="navigation">
  <div class="top-menu">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-2">
          <div id="fh5co-logo"><a href="index.php"><img src="images/logo2.svg" alt style="
    width: 50%;
    min-width: 130px;">
            </a></div>
        </div>
        <div class="col-xs-10 text-right menu-1">
          <ul>
            <li class="<?php if ($page_en_cours == 'Accueil') {echo "active";} ?>"><a href="index.php">Accueil</a></li>
            <li class="has-dropdown <?php if ($page_en_cours == 'Nos-services') {echo "active";} ?>">
              <a href="services.php">Nos services</a>
              <ul class="dropdown">
                <li><a href="#">Bureaux et Surfaces commerciales</a></li>
                <li><a href="#">Millieu Médical</a></li>
                <li><a href="#">Entrepots et Usines</a></li>
              </ul>
            </li>
            <li class="<?php if ($page_en_cours == 'About') {echo "active";} ?>"><a href="about.php" >Nous contacter</a></li>
            <li class="btn-cta"><a href="#"><span>Obtenir un devis</span></a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</nav>
