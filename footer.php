<footer id="fh5co-footer">
	<div class="container arbre-box">
		<div class="row row-pb-md" style="max-width: 100%;">
			<div class="col-md-4 fh5co-widget">
				<h4>Dynamic Services</h4>
				<p>Entreprise de nettoyage</p>
				<p class="second">Spécialisée dans l'entretien et la remise en état de tout type de locaux</p>
			</div>
			<div class="col-md-4 col-md-push-1">
				<h4>Liens</h4>
				<ul class="fh5co-footer-links">
					<li><a href="http://dynamic-services.fr">Accueil</a></li>
					<li><a href="http://dynamic-services.fr/services">Nos services</a></li>
					<li><a href="http://dynamic-services.fr/contact">Obtenir un devis</a></li>
				</ul>
			</div>

			<div class="col-md-4 col-md-push-1">
				<h4>Contact Information</h4>
				<ul class="fh5co-footer-links">
					<li>9b Rue des Dolmens, <br> 46220 Prayssac</li>
					<li><a href="tel://0615228540">+ 33 6 15 22 85 40</a></li>
					<li><a href="mailto:contact@dynamic-services.fr">contact@dynamic-services.fr</a></li>
				</ul>
			</div>

		</div>
	</div>
</footer>
<div class="gototop js-top active">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
