<!DOCTYPE HTML>
<html>
	<head>
	<link rel="canonical" href="http://dynamic-services.fr/contact.php"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dynamic Services | Contactez-nous</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Contactez-nous pour un devis ou des renseignements" />
	<meta name="keywords" content="Devis, nettoyage, prestation, contactez-nous" />
		<!-- <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">	 -->
	<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<?php $page_en_cours = 'Contact'; ?>
	<body>
		<div id="page">
		<div class="fh5co-loader" style="display: none;"></div>
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle fh5co-nav-white"><i></i></a>
		<div id="fh5co-offcanvas"><ul>
		            <li class="active"><a href="index.php">Accueil</a></li>
		            <li class=""><a href="services.php">Nos services</a></li>
		            <li class="btn-cta"><a href="contact.php"><span>Nous contacter</span></a></li>
		          </ul></div>
			<?php include("menu.php") ?>
  <div class="recrutement-titre">
    <h1>Contactez-<strong>Nous</strong></h1>
    <p>N’hésitez pas à remplir notre formulaire de contact ! Nous vous répondrons dans les plus brefs délais.</p>
  </div>

  <div id="form-central" class="container-fluid nopadding">
      <div class="container" style="max-width:100%;">
          <div class="row"  style="max-width:100%;">

                  <form method="post">
                      <div class="formRecrutement">
                          <div class="col-xs-12 col-sm-6 form-group">
                              <label class="control-label" for="email">E-mail *</label>
                              <input class="form-control" type="email" name="email" required="" id="email">
                          </div>
                          <div class="clear"></div>
                          <div class="col-xs-12 form-group">
                              <label class="control-label" for="motivation">Votre message *</label>
                              <textarea class="form-control" rows="4" cols="50" name="message" id="motivation" required=""></textarea>
                          </div>
                          <div class="col-xs-12 form-group text-rightX contact-submit" style="display: block;text-align: center;">
                              <input type="hidden" name="form-valid" value="1">
                              <input type="submit" class="btn btn-info btn-specific" value="Envoyer" name="submit">
                          </div>
                      </div>
                  </form>
									<?php
    if (isset($_POST['message'])) {
        $position_arobase = strpos($_POST['email'], '@');
        if ($position_arobase === false)
            echo '<p>Votre email doit comporter un arobase.</p>';
        else {
            $retour = mail('contact@dynamic-services.fr', 'Envoi depuis la page Contact', $_POST['message'], 'From: ' . $_POST['email']);
            if($retour)
                echo '<p>Votre message a été envoyé.</p>';
            else
                echo '<p>Erreur.</p>';
        }
    }
    ?>
									<ul class="liste-contact">
										<li>DYNMIC SERVICES</li>
										<li>contact@dynamic-services.fr</li>
										<li>06.15.22.85.40</li>
									</ul>
                          </div>
      </div>
  </div>


	<?php include("footer.php") ?>

</div>

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>
