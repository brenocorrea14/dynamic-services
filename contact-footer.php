<div id="fh5co-started" style="background-image: url(images/fond6.jpg);">
	<div class="overlay"></div>
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
				<h2>Vous Avez Des Questions ?</h2>
				<p>N’hésitez pas à remplir notre formulaire de contact ! Nous vous répondrons dans les plus  brefs délais. </p>
				<p><a href="http://dynamic-services.fr/services" class="btn btn-default btn-lg"><strong>Contactez nous</strong></a></p>
			</div>
		</div>
	</div>
</div>
