<!DOCTYPE HTML>
<html>
	<head>
	<link rel="canonical" href="http://dynamic-services.fr/services.php"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dynamic Services | Nos services</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Nos lieux d'intervention. Nous intervenons près de chez vous" />
	<meta name="keywords" content="bureaux, gîtes" />


	<!-- <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">	 -->
	<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<?php $page_en_cours = 'Nos-services'; ?>
	<body>
		<div class="fh5co-loader" style="display: none;"></div>
		<div id="page">

	<?php include("menu.php") ?>
	<div class="services-container">
		<div class="service-texte">
			<h1>Découvrez nos prestations d'entretien de locaux</h1>
			<p>Nos agents d’entretien interviennent dans les secteurs d’activités cités ci-dessous. Entreprise de nettoyage globale, DYNAMIC SERVICES est capable d’intervenir sur tous les types de nettoyage, que ce soit l’entretien d’une usine, d’une grande surface ou d’une copropriété.</p>
		</div>
			<?php
				 require('config.php');
		 $reponse = $bdd->query('SELECT *  FROM services ');

		 // On affiche chaque entrée une à une
		 while ($donnees = $reponse->fetch())
		 {
		 ?>
				<div class="col-md-6 nopadding animate-box same-height <?php echo $donnees['num'] ?>">
					<div class="service-contenu">

								<div class="service-flex">
									<img src="<?php echo $donnees['image'] ?>" alt="<?php echo $donnees['lieux']?>" class="img-services">
									<div class="">
										<h2><strong><?php echo $donnees['lieux']?></strong></h2>
										<p><strong><?php echo $donnees['contenu'] ?></strong></p>
									</div>
								</div>
					</div>
				</div>
		 <?php
		 }

		 $reponse->closeCursor(); // Termine le traitement de la requête

		 ?>
	</div>
	<?php include("contact-footer.php") ?>
	<?php include("footer.php") ?>
</div>


	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>
	<?php
		$service = $_GET['prestaion'];
	 ?>
	<script>
				$(document).ready(function (){
					var lieux = "."+"<?php echo $service ?>";
				                $('html, body').animate({
				                    scrollTop: $(lieux).offset().top
				                }, 2000);
				        });
	</script>
	</body>
</html>
