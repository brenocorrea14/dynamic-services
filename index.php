<!DOCTYPE HTML>
<html lang="fr">
<head>
<link rel="canonical" href="http://dynamic-services.fr"/>
<script data-ad-client="ca-pub-2409126454274486" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Entreprise de nettoyage pour les particuliers et professionnels </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Entreprise spécialisée dans le nettoyage, l'entretien et la remise en état de tout types de locaux. Professionnel et particulier | Dynamic Services">
<meta name="keywords" content="Dynamic Services, Entreprise de Nettoyage, Nettoyage particulier, Nettoyage professionnel, Entreprise de nettoyage Cahors, Entreprise de nettoyage Lot"/>
<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
<script src="js/modernizr-2.6.2.min.js"></script>


</head>
	<?php $page_en_cours = 'Accueil'; ?>
<body>

  <div class="fh5co-loader" style="display: none;"></div>
<div id="page">
  <div class="top-height">
<?php include("menu.php") ?>
<aside id="fh5co-hero" class="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">
	   	<li style="background-image: url(images/fond1.jpg);background-position-x: 100%;">
	   		<div class="overlay-gradient"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text slider-text-bg">
		   				<div class="slider-text-inner">
		   					<h1>Entreprise de nettoyage</h1>
								<p class="ct">Situé dans le Lot, <strong>Dynamic Services</strong> intervient à <strong>Cahors</strong> et ses alentours. Découvez nos services en cliquant <a href="#" class="scroll-services">ici<i class="icon-arrow-right"></i></a></p>
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	   	<li style="background-image: url(images/fond4.jpg);background-position-x: 85%;">
	   		<div class="overlay-gradient"></div>
	   		<div class="container-fluids">
	   			<div class="row">
		   			<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text slider-text-bg">
		   				<div class="slider-text-inner">
		   					<strong>Ensemble conservons l'environnement</strong>
								<p class="ct">Nous nous engageons à respecter à la lettre le mot <strong>propreté</strong> tout en maîtrisant les coûts <br><br><a href="#" class="scroll-ecolo">En savoir plus <i class="icon-arrow-right"></i></a></p>
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	  	</ul>
  	</div>
</aside>
</div>
<div id="fh5co-about">
	<div class="container">
		<div class="row entreprise">
			<div class="text-center">
				<h2>L'Entreprise</h2>
				<blockquote style="border-left: 5px solid rgba(212, 195, 4, 0.8);">
					<p class="paraphe-quotes"><strong>DYNAMIC SERVICES</strong> Une entreprise à taille humaine qui répond à vos attentes : nous mettons à votre service notre expérience et nos savoir-faire dans les domaines du nettoyage, de l'entretien et de la remise en état et nous nous engageons à fournir un travail de qualité, sérieux et efficace. <br><br>
					Que ce soit pour une intervention ponctuelle ou pour une collaboration sur le long terme, le niveau d’exigence et de professionnalisme dispensé par nos agents reste le même ! En effet, c’est ce souci du détail et de l’approche qualitative qui fait aujourd’hui la force de l’entreprise de nettoyage.</p>
				</blockquote>
			</div>
      </div>
		<div class="row member-container">
			<div class="member member1">
				<div class="col-md-6 nopadding animate-box member-img">
					<div class="author" style="background-image: url(images/Ecologique.jpg);margin: 20px;"></div>
				</div>
				<div class="col-md-6 animate-box member-text">
					<div class="desc desc1">
						<ul class="textBroom align-middle ">
								<li><i class="fa fa-leaf iconHeight"></i></li>
								<li><h3><strong>Ecologique</strong></h3></li>
								<li style="padding: 1rem;">
<p>Nos produits ne nuisent pas à l'environnement et respectent la norme de nettoyage écologique pour un monde plus durable</p></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="member member2">
			<div class="col-md-6 nopadding animate-box col-md-push-6 member-img">
				<div class="author" style="background-image: url(images/Economique.jpg);margin: 20px;"></div>
				</div>
				<div class="col-md-6 animate-box col-md-pull-6 member-text">
					<div class="desc desc2">
						<ul class="textBroom align-middle">
								<li><i class="fas fa-thumbs-up iconHeight"></i></li>
								<li><h3><strong>Economique</strong></h3></li>
								<li style="padding: 1rem;">
									<p>Nous laissons votre <strong>maison propre</strong> et désinfectée, rapidement, silencieusement, et sans dépasser votre budget.
									</p>
								</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="fh5co-mission">
	<div class="container">
      <div class="data-container">
        <h2>Nos lieux d'intervention</h2>
<p><strong>DYNAMIC SERVICE</strong> intervient auprès des particuliers et professionnels sur tous types d'espaces, de locaux et de surfaces :</p>
        <div class="slider">
    		  <?php
    		       require('config.php');
    		   $reponse = $bdd->query('SELECT *  FROM services ');
    		   // On affiche chaque entrée une à une
    		   while ($donnees = $reponse->fetch())
    		   {

    		   ?>
           <div class="card card-services" style="width: 18rem;">
      			<a href="http://dynamic-services.fr/services" rel="nofollow">
			 <div class="imgBox">
      				 <img src="<?php echo $donnees['image'] ?>" class="card-img-top" alt="<?php echo $donnees['lieux'] ?>">
               <h3 class="lieux"><strong><?php echo $donnees['lieux'] ?></strong></h3>
      			 </div>
      			 </a>
      		 </div>
    		   <?php
    		   }

    		   $reponse->closeCursor(); // Termine le traitement de la requête

    		   ?>
    		  </div>
		</div>
	</div>
</div>
<?php include("contact-footer.php") ?>
<?php include("footer.php") ?>
</div>

<!-- jQuery -->
 <script src="js/jquery.min.js"></script>
 <!-- jQuery Easing -->
 <script src="js/jquery.easing.1.3.js"></script>
 <!-- Bootstrap -->
 <script src="js/bootstrap.min.js"></script>
 <!-- Waypoints -->
 <script src="js/jquery.waypoints.min.js"></script>
 <!-- Flexslider -->
 <script src="js/jquery.flexslider-min.js"></script>
 <!-- Magnific Popup -->
 <script src="js/jquery.magnific-popup.min.js"></script>
 <script src="js/magnific-popup-options.js"></script>
 <!-- Main -->
 <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


 <script src="js/main.js"></script>
 <script src="slick/slick.min.js"></script>
 <script src="js/style.js"></script>

 <noscript>Your browser does not support JavaScript</noscript>
</body>
</html>
